# About
Just VLC player control on telnet connection. Video short review https://youtu.be/PHT2ZlBrWtA

For compile and upload Arduino sketch to NNM_2019 badge: copy `secret.example` to `secret.h` and set WiFi credentials and IP/port for VLC telnet host.

Setup Arduino IDE:
* Tools - Board - ESP32 Dev Module
* Tools - Port - /dev/ttyUSB0
* Tools - Serial Monitor - 115200 baud
* File - Preferences - https://img.in6k.com/screens/6d5c490a_6d5c490a.png (https://dl.espressif.com/dl/package_esp32_index.json)
* Manage Libary (Ctrl+Shift+I) add (https://img.in6k.com/screens/a3ae8721_a3ae8721.png):
    * Adafruit Circuit Playground
    * Adafruit DMA neopixel library
    * Adafruit NeoPixel
    * LibSSH-ESP32 (no need for now)


## run VLC on MacOS with ssh command:
```
ip="10.10.10.10"
port="9876"
ssh -o LogLevel=QUIET -n -f $user@$ip "sh -c '/Applications/VLC.app/Contents/MacOS/VLC file:////Users/$user/Downloads/Music/ -Z -L --quiet --recursive expand --control=rc --rc-host $ip:$port --novideo &'"
```


## Additional info: 
* https://gitlab.com/techmaker
* https://blog.techmaker.ua/posts/nonamecon-badge-2019/
* https://www.youtube.com/watch?v=chfoAWevHMs
* https://github.com/BlackVS/nnc2019-npm
* https://blog.techmaker.ua/posts/nonamecon-badge-2020/


## Credits
 
Igor Boloto (iboloto@gmail.com)
 
## License
 
The MIT License (MIT)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
