// ESP32 noname2019 badge Remote Control MusicPlayer
// VLC telnet control 
// gitlab.com/iboloto/playercontrolvlc_nnc2019
// MIT Licensed https://en.wikipedia.org/wiki/MIT_License
// Author: iboloto@gmail.com 
// Ping use (just for test)
// TODO ssh and volume control on MacOS
// If critical issue - reboot MacMini with ssh key
//SSH
//#include <DNSServer.h>
//#include "ESP32Ping.h"
//#include "libssh_esp32.h"
// Create and initialize SSH client object
//ESP32SSHCLIENT ssh_client;


// Telnet use https://www.dfrobot.com/blog-1003.html
#include <WiFi.h>
#include "secret.h"

int brightness=15;
int loops = 0;


// TODO: if don't touch anything set rainbow circle 
// TODO: flash green light for next track
int longwait = 0;
// Touch control variables
int i = 1;
int count = 0;
int vol = 50;

String telnetread = "";
String volstring = "";
String statestring = "";

int touch_value;
int last_touch;

//Leds
#include <Adafruit_NeoPixel.h>
int pin         =  4; 
int numPixels   = 10; 
int lednumber = 0;

int pixelFormat = NEO_GRB + NEO_KHZ800;
Adafruit_NeoPixel *pixels;
//#define DELAYVAL 500 // Time (in milliseconds) to pause between pixels


// Rotary Encoder Inputs
#define CLK GPIO_NUM_34
#define DT GPIO_NUM_17
#define SW GPIO_NUM_16
int counter = 0;
int currentStateCLK;
int lastStateCLK;
int RotateIsActive = 0;
//String lastDir ="CW";
int lastColor = 0;
int lastLed = 0;
String PauseIsActive ="false";
String currentDir ="";
unsigned long lastButtonPress = 0;
unsigned long check = 0;

// ColorTable.jpg
// ColorA is array with color code
// [0] blue
// [1] green
// [2] orange
// [3] red

int ColorA[4][3] = {
                {0, 0, brightness},
                {0, brightness, 0},
                {brightness, brightness/2, 0},
                {brightness, 0, 0}
                };

void led_clear(){
  for(int i=0; i<numPixels; i++) { 
    pixels->setPixelColor(i, pixels->Color(0, 0, 0));
    pixels->show();   // Send the updated pixel colors to the hardware.
  }
  }

void led_ring(int count, int bright){
  for(int i=0; i<numPixels; i++) { 
      pixels->setPixelColor(i, pixels->Color(0, 0, 0));
      }
   for(int i=0; i<=count; i++) { 
    pixels->setPixelColor(i, pixels->Color(0, 30+(bright*5), 5));
  }
   pixels->show();  // Send the updated pixel colors to the hardware.
  }

void all_led_color(int r, int g, int b){
   for(int i=0; i<numPixels; i++) { 
    pixels->setPixelColor(i, pixels->Color(r, g, b));
  }
  pixels->show();
}

void ReadVolumeLevel(){
    WiFiClient client;
    // Short blink red lights while not connect to telnet host and port  
   while (! client.connect(host, port)) {
   for(int i=0; i<numPixels; i++) { 
    pixels->setPixelColor(i, pixels->Color(brightness, 0, 0));
    pixels->show();   // Send the updated pixel colors to the hardware.
    }
   Serial.println("waitng");
   delay(50);
   led_clear();
   delay(1000);    
    }
    if (client.connect(host, port)) {
        telnetread = "";
        volstring = "";
        statestring = "";
        client.println("status");
        delay (50);
      while(client.available()){
            char c = client.read();
            telnetread += c;
            }
   for (int i = telnetread.lastIndexOf("audio volume")+14; i < telnetread.lastIndexOf("audio volume")+17; i++){
      if (isDigit(telnetread[i])){
      volstring += String(telnetread[i]);
    }
   }
    for (int i = telnetread.lastIndexOf("state ")+6; i < telnetread.lastIndexOf("state ")+10; i++) statestring += String(telnetread[i]);
    vol=volstring.toInt();
         }
//    Serial.print("Volume is: |");
//    Serial.print(volstring);
//    Serial.print("| State is |");
//    Serial.print(statestring);
//    Serial.println("|");
    }
  
void ChangeLevelLight(int way){
  
//https://habrastorage.org/getpro/geektimes/post_images/20f/6fd/26a/20f6fd26a02c9884d7d7db736f9057e2.jpg

 if (way==1){
  if (lastColor==3){
    lastLed++;
    lastColor=0;
  }
  else{
    lastColor++;
  }
 }
 else{
  if (lastColor==0){
    pixels->setPixelColor(lastLed, pixels->Color(0, 0, 0));
    lastLed--;
    lastColor=3;
  }
  else{
    lastColor--;
  }
 }
        pixels->setPixelColor(lastLed, pixels->Color(ColorA[lastColor][0], ColorA[lastColor][1], ColorA[lastColor][2]));
        pixels->show();

}

void VolumeLevelLight(int level){
  
//https://habrastorage.org/getpro/geektimes/post_images/20f/6fd/26a/20f6fd26a02c9884d7d7db736f9057e2.jpg

      if (level>320){ level=320; }
      if (level<1){ level=1; }
      int ledch=0;
    while (level>32){
      level = (level - 32);
      ledch++;
    }
 for(int i=0; i<ledch; i++) { 
    pixels->setPixelColor(i, pixels->Color(brightness, 0, 0));
  }
   for(int i=ledch; i<numPixels; i++) { 
    pixels->setPixelColor(i, pixels->Color(0, 0, 0));
  }

   switch (level) {
    // blue
      case 0 ... 7:
//        pixels->setPixelColor(ledch, pixels->Color(ColorA[1][1], ColorA[1][2], ColorA[1][3]));
        lastColor=0;
        break;
    // green        
      case 8 ... 15:
  //      pixels->setPixelColor(ledch, pixels->Color(0, brightness, 0));
        lastColor=1;
        break;
    // orange        
      case 16 ... 23:
//        pixels->setPixelColor(ledch, pixels->Color(brightness, brightness/2, 0));
        lastColor=2;
        break;
    // red        
      case 24 ... 32:
//        pixels->setPixelColor(ledch, pixels->Color(brightness, 0, 0));
        lastColor=3;
        break;
   }
          pixels->setPixelColor(ledch, pixels->Color(ColorA[lastColor][0], ColorA[lastColor][1], ColorA[lastColor][2]));
          pixels->show();
    lastLed = ledch;
//    Serial.print("Volume is: |");
//    Serial.print(volstring);
//    Serial.print("| brightness is |");
//    Serial.print(brightness);
//    Serial.print("| led is |");
//    Serial.print(ledch);
//    Serial.print("| level is |");
//    Serial.print(level);
//    Serial.print("| lastColor is |");
//    Serial.print(lastColor);
//    Serial.print("| 1 is |");
//    Serial.print(ColorA[lastColor][1]);
//    Serial.print("| 2 is |");
//    Serial.print(ColorA[lastColor][2]);
//    Serial.print("| 3 is |");
//    Serial.print(ColorA[lastColor][3]);
//    Serial.println("|");
}


void setup()
{
// Set encoder pins as inputs
  pinMode(CLK,INPUT);
  pinMode(DT,INPUT);
  pinMode(SW, INPUT_PULLUP);

// Leds
  pixels = new Adafruit_NeoPixel(numPixels, pin, pixelFormat);
  pixels->begin(); // INITIALIZE NeoPixel strip object (REQUIRED)

  Serial.begin(115200);
  delay(1000); // give me time to bring up serial monitor
  // Read the initial state of CLK
  lastStateCLK = digitalRead(CLK);
  last_touch = touchRead(T5);
  WiFi.begin(ssid, password);
  Serial.println("ESP32 noname2019 badge Remote Control MusicPlayer");
  while (WiFi.status() != WL_CONNECTED) {
   led_clear();
   for(int i=1; i<numPixels; i++) { 
    pixels->setPixelColor(i, pixels->Color(brightness, brightness, 0));
    pixels->show();   // Send the updated pixel colors to the hardware.
    i++;
  }
   delay(500);
   led_clear();
   for(int i=0; i<numPixels; i++) { 
    pixels->setPixelColor(i, pixels->Color(brightness, brightness, 0));
    pixels->show();   // Send the updated pixel colors to the hardware.
    i++;
  }
  delay(500);
   Serial.println("WiFi connecting...");
  }
  
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("Hostname: ");
  Serial.println(WiFi.getHostname());
  Serial.print("ESP Mac Address: ");
  Serial.println(WiFi.macAddress());
  Serial.print("Subnet Mask: ");
  Serial.println(WiFi.subnetMask());
  Serial.print("Gateway IP: ");
  Serial.println(WiFi.gatewayIP());
  Serial.print("DNS: ");
  Serial.println(WiFi.dnsIP());
  
  Serial.println("WiFi connected!");
  delay(1000);
  led_clear();
  
// give the WiFi a second to initialize:
    WiFiClient client;
 
// double blink green all lights if wifi and telnet is ok
    if (client.connect(host, port)) {
    delay(250);    
    led_clear();
    delay(250);
    all_led_color(0, brightness*2, 0);
    delay(250);    
    led_clear();
    delay(250);    
    all_led_color(0, brightness*2, 0);
    delay(250); 
    led_clear();   
    counter=0;
    }
// Read from telnet twice )    
    ReadVolumeLevel();
    ReadVolumeLevel();
    if (vol>=0) VolumeLevelLight(vol);
    if (statestring == "paus") all_led_color(0, 0, brightness+5);
}



void loop()
{
  WiFiClient client;

  // Read the current state of CLK
    currentStateCLK = digitalRead(CLK);
  // If last and current state of CLK are different, then pulse occurred
  // React to only 1 state change to avoid double count
  if (currentStateCLK != lastStateCLK  && currentStateCLK == 1){
    // If the DT state is different than the CLK state then
    // the encoder is rotating CCW so decrement
    // Send rotation to telnet client volup or voldown
    if (digitalRead(DT) != currentStateCLK) {
      counter --;
      currentDir ="CCW";
      vol=vol-8;
      ChangeLevelLight(0);
//      VolumeLevelLight(vol);
//    client.println("voldown");
//    client.println();
    } else {
      // Encoder is rotating CW so increment
      counter ++;
      currentDir ="CW";
      vol=vol+8;
      ChangeLevelLight(1);
//      VolumeLevelLight(vol);
//    client.println("volup");
//    client.println();
    }
    RotateIsActive=1;
    loops=0;
//    Serial.print("Counter: ");
//    Serial.print(counter);
//    Serial.print(" | Vol is: ");
//    Serial.print(vol);
//    Serial.print(" | currentDir : ");
//    Serial.print(currentDir);
// Remember last dir not use for now.
//  lastDir = currentDir;
  }

  // Remember last CLK state
  lastStateCLK = currentStateCLK;
  // Read the button state
  int btnState = digitalRead(SW);
  //If we detect LOW signal, button is pressed
    if (btnState == LOW) {
    //if 50ms have passed since last LOW pulse, it means that the
    //button has been pressed, released and pressed again
    check++;
    if (check == 700) {
      if (brightness==15){
        brightness=70;
      }
        else{
        brightness=15;
        }
    Serial.print("Long press! lastButtonPress=");
    Serial.print(lastButtonPress);
    Serial.print("! check=");
    Serial.print(check);
    Serial.print("! brightness=");
    Serial.print(brightness);
      ReadVolumeLevel();
      if (statestring == "paus"){
          all_led_color(0, 0, brightness+5);
        }
        else{
      if (vol>=0) VolumeLevelLight(vol);
      }
    }
    if (millis() - lastButtonPress > 200) {
    Serial.print("Button pressed! lastButtonPress=");
    Serial.println(lastButtonPress);
      if (client.connect(host, port)) {
        client.println("pause");
        client.println();
        Serial.println("Toggle pause");
        longwait=0;
//        delay(100);
      ReadVolumeLevel();
      ReadVolumeLevel();
      if (statestring == "paus"){
//        PauseIsActive = "true";
          all_led_color(0, 0, brightness+5);
        }
        else{
      if (vol>=0) VolumeLevelLight(vol);
      }
      }
    }
    // Remember last button press event
    lastButtonPress = millis();
   }
   else{
    check=0;
   }


// touch_value just for stable touchRead function. I don't know why it's need.
   touch_value = touchRead(T5);
 if (touchRead(T5) < 70){
// touch hold count
  last_touch = touch_value;
  count = count + 1;
  delay(100);
// count 5 will be if long press touch. Play next track. And wait 3,5 sec for untouch
    if (count == 5){
    if (client.connect(host, port)) {
    client.println("next");
    client.println();
    Serial.println("Next track");
    all_led_color(0, brightness, 0);
    delay(300);
      }
    ReadVolumeLevel();
    ReadVolumeLevel();
    if (vol>=0) VolumeLevelLight(vol);
   delay(2200);
   count = 0;
   longwait=0;
   return;
  }
  delay(5);
 }
 // If untouch and wait +1 touch (compare with > 1)
  else{
   count = 0;
  }
  delay(1);
  touch_value = touchRead(T5);

// count loop cycle
  loops++;

// wait 50 loops after last rotate and change volume
  if (RotateIsActive == 1 && loops == 50){
    RotateIsActive=0;
    longwait=0;
//    Serial.println("It was rotate loops is 50");
      if (client.connect(host, port)) {
    client.print("volume ");
    client.println(vol);
    client.println();
      }
  }
  //Wait some loops and reread Volume level for update LedsRing
  if (loops>2000){
      ReadVolumeLevel();
      if (vol>=0) VolumeLevelLight(vol);
      if (statestring == "paus") all_led_color(0, 0, brightness+5);
      loops=0;
      longwait++;
    }

  }
